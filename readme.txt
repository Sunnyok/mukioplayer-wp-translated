﻿Mukioplayer-for-Wordpress Translated!

Plugin by aristotle9, licensed under GNU GPL v2
UI translated to English by Sunnyok 

Latest translated version is 1.6 (Oct 21, 2011).

Instructions:
	1. Requirement:
	   - php_pdo_sqlite module(for saving Danmaku)
	   - WordPress installation after version 2.8
	2. Plugin setup: Find the Mukioplayer tab in settings, select category you want the player to appear in and click save.
	                 * Remember to rename source folder to "mukioplayer-for-wordpress" in order for the plugin to function.
	3. Insert video: Make new post and click on the MQ icon to add video.

Additional Notice:
	1. Only the default wordpress player has been translated... not the original yet.
	2. Player has to be bound to a category in order for it to appear in a post of that category.
	3. Comment ID will be generated when the video post is published or republished.
	4. Comment ID will not be generated (I think) when 'disable commenting' is selected in player settings. 
	   So republish post after enabling commenting to generate comment ID.
	5. To check whether the plugin is properly setup, go to 'All Posts' and click the 'comment' sub-menu
	   under the post title to see if you can get to the comment manage interface.
	6. Anyways, just check you settings.


More information can be found on PDF at https://code.google.com/p/mukioplayer4wp/downloads/detail?name=mukioplayer4wp-v1.0a.pdf

* PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION. (GNU GPL v2) 