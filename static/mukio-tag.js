(function(){
  // creates the plugin
  tinymce.create('tinymce.plugins.mukiotag', {
    // creates control instances based on the control's id.
    // our button's id is "mukiotag_button"
    createControl : function(id, controlManager) {
      if (id == 'mukiotag_button') {
        // creates the button
        var button = controlManager.createButton('mukiotag_button', {
          // title : '[mukio title=\'title\' desc=\'description\' width=\'width\' height=\'height\']vid=videoID&type=videoSource[/mukio]', // title of the button
          title : 'Insert Video', // title of the button
          image : '../wp-content/plugins/mukioplayer-for-wordpress/static/mqbt.gif"',  // path to the button's image
          onclick : function() {
            // triggers the thickbox
            var width = jQuery(window).width(), H = jQuery(window).height(), W = ( 720 < width ) ? 720 : width;
            W = W - 80;
            H = H - 84;
            tb_show( 'Insert Video:', '#TB_inline?width=' + W + '&height=' + H + '&inlineId=mukio-fm' );
            // var shortcode  = '[mukio title=\'ttt\']vid=xxx&type=yyy[/mukio]<br />';
            // tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);
          }
        });
        return button;
      }
      return null;
    }
  });
  // registers the plugin. DON'T MISS THIS STEP!!!
  tinymce.PluginManager.add('mukiotag', tinymce.plugins.mukiotag);
  //做静态表单
  jQuery(function (){
    var form = jQuery("" + 
"      <div id='mukio-fm'>" + 
"        <table id='mukio-tb' cellspacing='0' style='width:100%'>" + 
"          <thead>" + 
"            <tr>" + 
"            <th style='width:30%;' scope='col'></th><th scope='col'></th>" + 
"            </tr>" + 
"          </thead>" + 
"          <tbody>" + 
"          <tr>" + 
"            <th><label for='mukio-title'>Title</label></th>" + 
"            <td>" + 
"            <input type='text' id='mukio-title' name='mukio-title' value='' size='50' />*<br />" + 
"            <small>Required when post contains multiple media</small>" + 
"            </td>" + 
"          </tr>" + 
"          <tr>" + 
"            <th><label for='mukio-desc'>Description</label></th>" + 
"            <td>" + 
"            <input type='text' id='mukio-desc' name='mukio-desc' value='' size='50' /><br />" + 
"            <small>Will be displayed when media is selected</small>" + 
"            </td>" + 
"          </tr>" + 
"          <tr>" + 
"            <th><label for='mukio-type'>Source</label></th>" + 
"            <td>" + 
"              <select id='mukio-type' name='mukio-type'>" + 
"                <option value='sina' selected='selected'>sina</option>" + 
"                <option value='qq'>qq</option>" + 
"                <option value='youku'>youku</option>" + 
"                <option value='video'>video source</option>" + 
"                <option value='sound'>mp3 source</option>" + 
"              </select>" + 
"              <br />" + 
"              <small>Local upload select \"video source\" or \"mp3 source\"</small>" + 
"            </td>" + 
"          </tr>" + 
"          <tr id='mukio-vid-r'>" + 
"            <th><label for='mukio-vid'>Video ID</label></th>" + 
"            <td>" + 
"            <input type='text' id='mukio-vid' name='mukio-vid' value='' size='50' />*<br />" + 
"            <small>ID can be found at video player's query string</small>" + 
"            </td>" + 
"          </tr>" + 
"          <tr id='mukio-file-r'>" + 
"            <th><label for='mukio-file'>File URL</label></th>" + 
"            <td>" + 
"            <input type='text' id='mukio-file' name='mukio-file' value='' size='50' />*<br />" + 
"            <small>Please fill in full address</small>" + 
"            </td>" + 
"          </tr>" + 
"          <tr id='mukio-cid-r'>" + 
"            <th><label for='mukio-cid'>Comment ID</label></th>" + 
"            <td>" + 
"            <input type='text' id='mukio-cid' name='mukio-cid' value='' size='50' />*<br />" + 
"            <small>Make it up but don't repeat with others, or <input type='button' class='button' id='mukio-rndcid' name='mukio-rndcid' value='generate md5 from source' /></small>" + 
"            </td>" + 
"          </tr>" + 
"          <tr>" + 
"            <th><label for='mukio-width'>Player Width</label></th>" + 
"            <td>" + 
"            <input type='text' id='mukio-width' name='mukio-width' value='' /><br />" + 
"            <small>Leave empty for default width</small>" + 
"            </td>" + 
"          </tr>" + 
"          <tr>" + 
"            <th><label for='mukio-height'>Player Height</label></th>" + 
"            <td>" + 
"            <input type='text' id='mukio-height' name='mukio-height' value='' /><br />" + 
"            <small>Leave empty for default height</small>" + 
"            </td>" + 
"          </tr>" + 
"          <tr>" + 
"            <th></th>" + 
"            <td>" + 
"              <small>Relaunch this pop-up to insert more.</small><br /><br />" + 
"              <input type='button' id='mukio-submit' class='button-primary' value='Insert' name='submit' />" + 
"            </td>" + 
"          </tr>" + 
"          </tbody>" + 
"        </table>" + 
"      </div>" + 
"    ");
    var table = form.find('table');
    form.appendTo('body').hide();
    
    form.find('th').each(function(){
      jQuery(this).css('text-align','right').css('padding','0 30px 0 0');
    });
    form.find('tbody tr th,tbody tr td').each(function(){
      jQuery(this).css('border-width','2px 0 0').css('border-color','#eee').css('border-style','solid')
                   .css('padding-top','0.5em').css('padding-bottom','0.5em').css('margin','0.5em 0');
    });
    table.find('#mukio-rndcid').click(function(){
      table.find('#mukio-cid').val(jQuery.md5(table.find('#mukio-file').val()));
    });
    
    var rvid  = form.find('#mukio-vid-r');
    var rcid  = form.find('#mukio-cid-r').hide();
    var rfile = form.find('#mukio-file-r').hide();
    
    form.find('#mukio-type').change(function (){
      var v = jQuery(this).val();
      if (v == 'video' || v == 'sound') {
        rvid.hide();
        rfile.show();
        rcid.show();
      }
      else
      {
        rcid.hide();
        rfile.hide();
        rvid.show();
      }
    });
  
    form.find('#mukio-submit').click(function(){
    // defines the options and their default values
    // again, this is not the most elegant way to do this
    // but well, this gets the job done nonetheless
    var options = { 
      'width':'',
      'height':'',
      'title':'',
      'desc':'',
      'vid':'',
      'cid':'',
      'type':'',
      'file':''
      };
    for( var index in options) {
      options[index] = table.find('#mukio-' + index).val();
      if (index != 'type') {
        table.find('#mukio-' + index).val('');
      }
      // attaches the attribute to the shortcode only if it's different from the default value
      // if ( value !== options[index] )
        // shortcode += ' ' + index + '="' + value + '"';
    }
    var my_escape = function (str) {
      str += '';
      str = str.replace(/\[/g,'【');
      str = str.replace(/\]/g,'】');
      str = str.replace(/'/g,'"');
      return str;
    };
    var shortcode = '[mukio';
    if (options['title'] != '') {
      shortcode += " title='" + my_escape(options['title']) + "'";
    }
    if (options['desc'] != '') {
      shortcode += " desc='" + my_escape(options['desc']) + "'";
    }
    if (options['width'] != '') {
      shortcode += " width='" + parseInt(options['width']) + "'";
    }
    if (options['height'] != '') {
      shortcode += " height='" + parseInt(options['height']) + "'";
    }
    shortcode += ']';
    
    if (options['type'] == 'video' || options['type'] == 'sound') {
      shortcode += 'file=' + options['file'] + '&type=' + options['type'] +'&cid=' + options['cid'];
    }
    else {
      shortcode += 'vid=' + options['vid'] + (options['type'] == 'sina' ? '' : '&type=' + options['type']);
    }
    shortcode += '[/mukio]<br />';
    // inserts the shortcode into the active editor
    tinyMCE.activeEditor.execCommand('mceInsertContent', 0, shortcode);
    // closes Thickbox
    tb_remove();
    
    });//onclick
  });//jQuery
})()//function()